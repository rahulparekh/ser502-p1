# Let's define some functions
square = (x) -> x * x
cube   = (x) -> square(x) * x

# let's create arrays and objects. Pretty similar to JS 
song = ["do", "re", "mi", "fa", "so"]
singers = {Jagger: "Rock", Elvis: "Roll"}


# now let's create some arrays with additional coffeescript sugar added to it

# no commas needed if properties are on separate lines
bitlist = [
  1, 0, 1
  0, 0, 1
  1, 1, 0
]

# no need for the ugly braces!
kids =
    brother:
        name: "Max"
        age:  11
    sister:
        name: "Ida"
        age:  9


# array slicing and splicing
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

start   = numbers[0...2] # gives [1, 2]

middle  = numbers[3...-2] # gives [4, 5, 6, 7]

end     = numbers[-2..] # gives [8, 9]

copy    = numbers[..] # gives [1, 2, 3, 4, 5, 6, 7, 8, 9]


# if/else statements
isAwesome = isCool = true

# single line if statement. Notice lack of those pesky brackets
alert "Awesomeness found" if isAwesome

# python like if/else statement
if isAwesome and isCool
  alert "You are awesome and cool!"
else
  alert "Nope. You're missing something"

# more readable ternary operator use
awesomeness = if isAwesome then "Awesome!" else "Not Awesome :("


# loops

# simple loop
alert food for food in ['toast', 'cheese', 'wine']

# simple generated loop with increment
countdown = (x for x in [0..10] by 2)

# loop through an object
yearsOld = max: 10, ida: 9, tim: 11
age = for child, age of yearsOld
  "#{child} is #{age}"


# Destructuring Assignment
theBait   = 1000
theSwitch = 0

[theBait, theSwitch] = [theSwitch, theBait]

# Thank You!