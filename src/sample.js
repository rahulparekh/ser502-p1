var age, awesomeness, bitlist, child, copy, countdown, cube, end, food, isAwesome, isCool, kids, middle, numbers, singers, song, square, start, theBait, theSwitch, x, yearsOld, _i, _len, _ref, _ref1;

square = function(x) {
  return x * x;
};

cube = function(x) {
  return square(x) * x;
};

song = ["do", "re", "mi", "fa", "so"];

singers = {
  Jagger: "Rock",
  Elvis: "Roll"
};

bitlist = [1, 0, 1, 0, 0, 1, 1, 1, 0];

kids = {
  brother: {
    name: "Max",
    age: 11
  },
  sister: {
    name: "Ida",
    age: 9
  }
};

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

start = numbers.slice(0, 2);

middle = numbers.slice(3, -2);

end = numbers.slice(-2);

copy = numbers.slice(0);

isAwesome = isCool = true;

if (isAwesome) {
  alert("Awesomeness found");
}

if (isAwesome && isCool) {
  alert("You are awesome and cool!");
} else {
  alert("Nope. You're missing something");
}

awesomeness = isAwesome ? "Awesome!" : "Not Awesome :(";

_ref = ['toast', 'cheese', 'wine'];
for (_i = 0, _len = _ref.length; _i < _len; _i++) {
  food = _ref[_i];
  alert(food);
}

countdown = (function() {
  var _j, _results;
  _results = [];
  for (x = _j = 0; _j <= 10; x = _j += 2) {
    _results.push(x);
  }
  return _results;
})();

yearsOld = {
  max: 10,
  ida: 9,
  tim: 11
};

age = (function() {
  var _results;
  _results = [];
  for (child in yearsOld) {
    age = yearsOld[child];
    _results.push("" + child + " is " + age);
  }
  return _results;
})();

theBait = 1000;

theSwitch = 0;

_ref1 = [theSwitch, theBait], theBait = _ref1[0], theSwitch = _ref1[1];
