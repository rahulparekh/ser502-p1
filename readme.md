## Team 7
- Chinmay Dhekne
- Rahul Parekh
- Priyanka Vats

## Presentation 
https://www.youtube.com/watch?v=IysceE8w5eQ

## Installation Instructions for CoffeeScript:
If you have the node package manager, npm, installed:

```shell
npm install -g coffee-script
```

Leave off the `-g` if you don't wish to install globally. If you don't wish to use npm:                 




```shell
git clone https://github.com/jashkenas/coffeescript.git
sudo coffeescript/bin/cake install
```


If you wish to use a tool which auto-compiles CoffeeScript to JS on save, we recommend https://prepros.io/

These tasks can also be automated using Grunt.js or Gulp.js

